package javastory.club.stage3.step3.service;

public interface ServiceLycler {
	//
	BoardService createBoardService();
	ClubService createClubService();
	MemberService createMemberService();
	PostingService createPostingService();
}